// let rString="ASDFGHJawerty";
// let ar = rString.split('');
// console.log(ar);
// // console.log('a'.toUpperCase());
// let result = {};
// result = ar.reduce((acc, ele) => {
//     if((ele) in result){
//         result[ele] += +1;
//     }
//     else{
//         result[ele] = +1;
//     }
//     return result;
// },{});
// console.log(result);

// let nestedObj = {
//     obj2: {
//         arr1: [1, 2, 4, {
//             'nested value': [5,6,7]
//         }]
//     },
//     obj3: {
//         obj4: {
//             obj5: {
//                 'nested object':'This is a nested object value'
//             }
//         },
//         arr2:[true, 'Value inside arr2']
//     }
// }

// 1. Access the value of 'nested value' key.
// 2. Access the value of 7.
// 3. Access the value of obj5 key.
// 4. Access the value of 'nested object' key.
// 5. Access the value of 'value inside arr2'


// console.log(nestedObj.obj2.arr1[3]["nested value"]);

// console.log(nestedObj.obj2.arr1[3]["nested value"][2]);

// console.log(nestedObj.obj3.obj4.obj5);

// console.log(nestedObj.obj3.obj4.obj5['nested object']);


// console.log(nestedObj.obj3.arr2[1]);

// const objNew = {
//     key: 'value'
// }
// objNew[key]


const books = [
    { id: 1, title: "The Great Gatsby", author: "F. Scott Fitzgerald", year: 1925, genre: "Fiction", rating: 4.2 },
    { id: 2, title: "To Kill a Mockingbird", author: "Harper Lee", year: 1960, genre: "Fiction", rating: 4.5 },
    { id: 3, title: "Pride and Prejudice", author: "Jane Austen", year: 1813, genre: "Fiction", rating: 4.0 },
    { id: 4, title: "1984", author: "George Orwell", year: 1949, genre: "Fiction", rating: 4.3 },
    { id: 5, title: "The Catcher in the Rye", author: "J.D. Salinger", year: 1951, genre: "Fiction", rating: 4.1 },
    { id: 6, title: "To the Lighthouse", author: "Virginia Woolf", year: 1927, genre: "Fiction", rating: 4.4 }
];


//   Write a function getBooksByRating(books, minRating) that takes in an array of book objects and a minimum rating as arguments. The function should return an array of book titles that have a rating equal to or higher than the specified minimum rating.

// let minRat = 4.2;

// function getBooksByRating(books, minRating) {
//     let result = books.filter((ele) => {
//         if(ele.rating >= minRating){
//             return true;
//         }
//         else{
//             return false;
//         }
//     });
//     console.log(result);
// }

// getBooksByRating(books,minRat);

const dataset = {
    id: 1,
    name: "Nested Dataset",
    version: "1.0",
    description: "A dataset with deeply nested arrays and objects",
    data: [
      {
        id: 1,
        name: "John",
        age: 25,
        contacts: {
          email: "john@example.com",
          phone: {
            home: "123-456-7890",
            mobile: "987-654-3210"
          }
        },
        addresses: [
          {
            type: "home",
            street: "123 Main St",
            city: "New York",
            country: "USA"
          },
          {
            type: "work",
            street: "456 Elm St",
            city: "San Francisco",
            country: "USA"
          }
        ],
        orders: [
          {
            id: 1001,
            date: "2023-06-01",
            items: [
              { name: "Item 1", quantity: 2 },
              { name: "Item 2", quantity: 1 }
            ]
          },
          {
            id: 1002,
            date: "2023-06-15",
            items: [
              { name: "Item 3", quantity: 3 },
              { name: "Item 4", quantity: 1 },
              { name: "Item 5", quantity: 2 }
            ]
          }
        ]
      },
      {
        id: 2,
        name: "Jane",
        age: 30,
        contacts: {
          email: "jane@example.com",
          phone: {
            home: "111-222-3333",
            mobile: "444-555-6666"
          }
        },
        addresses: [
          {
            type: "home",
            street: "789 Oak St",
            city: "London",
            country: "UK"
          }
        ],
        orders: [
          {
            id: 1003,
            date: "2023-07-02",
            items: [
              { name: "Item 6", quantity: 1 }
            ]
          }
        ]
      }
    ]
  };


// console.log(dataset.data);


const itemsData = dataset.data.map(ele =>
    ele.orders.map(order =>
      order.items.map(item => ({
        name: item.name,
        quantity: item.quantity
      }))
    )
  ).flat(2);
  console.log(itemsData);